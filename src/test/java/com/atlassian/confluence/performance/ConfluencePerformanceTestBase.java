package com.atlassian.confluence.performance;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.util.Random;

import org.junit.BeforeClass;

import com.atlassian.confluence.performance.persona.Login;
import com.atlassian.confluence.performance.persona.Logout;
import com.atlassian.confluence.performance.persona.Searcher;
import com.atlassian.confluence.performance.persona.create.BlogCreator;
import com.atlassian.confluence.performance.persona.create.PageCreator;
import com.atlassian.confluence.performance.persona.create.SpaceCreator;
import com.atlassian.confluence.performance.persona.view.DashboardViewer;
import com.atlassian.confluence.performance.persona.view.PageViewer;
import com.atlassian.confluence.performance.persona.view.SpaceBrowser;
import com.atlassian.confluence.performance.persona.view.SpaceViewer;
import com.atlassian.confluence.performance.util.CustomLogger;
import com.atlassian.confluence.performance.util.TestConstants;

public class ConfluencePerformanceTestBase {
	protected Login loginWarmUp = new Login();
	protected Logout logoutWarmUp = new Logout();
	protected PageViewer pageViewerWarmUp = new PageViewer();
	protected PageViewer pageViewerMacroWarmUp = new PageViewer();
	protected PageCreator pageCreatorWarmUp = new PageCreator();
	protected PageCreator pageCreatorMacrosWarmUp = new PageCreator();
	protected BlogCreator blogCreatorWarmUp = new BlogCreator();
	protected SpaceCreator spaceCreatorWarmUp = new SpaceCreator();
	protected SpaceViewer spaceViewerWarmUp = new SpaceViewer();
	protected SpaceBrowser spaceBrowserWarmUp = new SpaceBrowser();
	protected DashboardViewer dashboardViewerWarmUp = new DashboardViewer();
	protected Login login = new Login();
	protected Logout logout = new Logout();
	protected PageViewer pageViewer = new PageViewer();
	protected PageViewer pageViewerMacro = new PageViewer();
	protected PageCreator pageCreator = new PageCreator();
	protected PageCreator pageCreatorMacros = new PageCreator();
	protected BlogCreator blogCreator = new BlogCreator();
	protected SpaceCreator spaceCreator = new SpaceCreator();
	protected SpaceViewer spaceViewer = new SpaceViewer();
	protected SpaceBrowser spaceBrowser = new SpaceBrowser();
	protected DashboardViewer dashboardViewer = new DashboardViewer();
	protected Searcher searcher = new Searcher();

	@BeforeClass
	public static void setup() throws Exception {
		TestConstants.BASE_URL = System.getProperty("base-url", "http://localhost:8080/confluence") + "/";
	}
	
	protected void report() throws Exception {
		CustomLogger.getLogger().info("");
		CustomLogger.getLogger().info("********************** Warmup ***************************");
		CustomLogger.getLogger().info("Login : " + loginWarmUp.getMeanInMS());
		CustomLogger.getLogger().info("Create page : " + pageCreatorWarmUp.getMeanInMS());
		CustomLogger.getLogger().info("Create Page with macro : " + pageCreatorMacrosWarmUp.getMeanInMS());
		CustomLogger.getLogger().info("Create blog : " + blogCreatorWarmUp.getMeanInMS());
		CustomLogger.getLogger().info("Create Space : " + spaceCreatorWarmUp.getMeanInMS());
		CustomLogger.getLogger().info("View page : " + pageViewerWarmUp.getMeanInMS());
		CustomLogger.getLogger().info("View page with macro : " + pageViewerMacroWarmUp.getMeanInMS());
		CustomLogger.getLogger().info("View Space : " + spaceViewerWarmUp.getMeanInMS());
		CustomLogger.getLogger().info("Space browser : " + spaceBrowserWarmUp.getMeanInMS());
		CustomLogger.getLogger().info("View Dashboard : " + dashboardViewerWarmUp.getMeanInMS());
		CustomLogger.getLogger().info("Logout : " + logoutWarmUp.getMeanInMS());
		CustomLogger.getLogger().info("*******************************************************");
		CustomLogger.getLogger().info("");
		CustomLogger.getLogger().info("********************** Execution ***************************");
		CustomLogger.getLogger().info("Login : " + login.getMeanInMS());
		CustomLogger.getLogger().info("Create page : " + pageCreator.getMeanInMS());
		CustomLogger.getLogger().info("Create Page with macro : " + pageCreatorMacros.getMeanInMS());
		CustomLogger.getLogger().info("Create blog : " + blogCreator.getMeanInMS());
		CustomLogger.getLogger().info("Create Space : " + spaceCreator.getMeanInMS());
		CustomLogger.getLogger().info("View page : " + pageViewer.getMeanInMS());
		CustomLogger.getLogger().info("View page with macro : " + pageViewerMacro.getMeanInMS());
		CustomLogger.getLogger().info("View Space : " + spaceViewer.getMeanInMS());
		CustomLogger.getLogger().info("Space browser : " + spaceBrowser.getMeanInMS());
		CustomLogger.getLogger().info("View Dashboard : " + dashboardViewer.getMeanInMS());
		CustomLogger.getLogger().info("Search : " + searcher.getMeanInMS());
		CustomLogger.getLogger().info("Logout : " + logout.getMeanInMS());
		CustomLogger.getLogger().info("*******************************************************");
	}

	protected void populateCreatePageData(PageCreator pageCreator, int count) {
		while (count > 0) {
			pageCreator.addPage("HL", "With+Image+" + System.nanoTime(), "fdfgdhgfhfhjfhjfhj");
			pageCreator.addPage("MS", "Hello+Nikhil" + System.nanoTime(), "646545645645ffgfdgdhgdh");
			pageCreator.addPage("HL", "Java+is+good" + System.nanoTime(), "234rfdsfgdtrgfdbvfdy");
			pageCreator.addPage("MS", "My+Page" + System.nanoTime(), "vcxftvcbxcdasdaf");
			pageCreator.addPage("HL", "With+Image+" + System.nanoTime(), "fdfgdhgfhfhjfhjfhj");
			pageCreator.addPage("MS", "Hello+Nikhil" + System.nanoTime(), "646545645645ffgfdgdhgdh");
			pageCreator.addPage("HL", "Java+is+good" + System.nanoTime(), "234rfdsfgdtrgfdbvfdy");
			pageCreator.addPage("MS", "My+Page" + System.nanoTime(), "vcxftvcbxcdasdaf");
			count--;
		}

		pageCreator.addPage("HL", "With Images", "fdfgdhgfhfhjfhjfhj");
		pageCreator.addPage("MS", "Hello Nikhil", "646545645645ffgfdgdhgdh");
		pageCreator.addPage("HL", "Java is good", "234rfdsfgdtrgfdbvfdy");
		pageCreator.addPage("MS", "My Page", "vcxftvcbxcdasdaf");
	}

	protected void populateCreatePageWithMacros(PageCreator pageCreator, PageViewer pageViewer, int count) {
		Random random = new Random();
		while (count > 0) {
			String pageName = "Roadmap" + random.nextInt(5000);
			pageCreatorMacros.addPage("HL", pageName, TestConstants.DATA.getRoadMapContent());
			pageViewer.addPage("HL", pageName);
			pageName = "PageTree" + random.nextInt(5000);
			pageCreatorMacros.addPage("MS", pageName, TestConstants.DATA.getPageTreeContent());
			pageViewer.addPage("MS", pageName);
			pageName = "Mixed" + random.nextInt(5000);
			pageCreatorMacros.addPage("MS", pageName, TestConstants.DATA.getMixedContent());
			pageViewer.addPage("MS", pageName);
			count--;
		}
	}

	protected void populateCreateBlogData(BlogCreator blogCreator, int count) {
		while (count > 0) {
			blogCreator.addBlog("HL", "With+Image+" + System.nanoTime(), "fdfgdhgfhfhjfhjfhj");
			blogCreator.addBlog("MS", "Hello+Nikhil" + System.nanoTime(), "646545645645ffgfdgdhgdh");
			blogCreator.addBlog("HL", "Java+is+good" + System.nanoTime(), "234rfdsfgdtrgfdbvfdy");
			blogCreator.addBlog("MS", "My+Page" + System.nanoTime(), "vcxftvcbxcdasdaf");
			blogCreator.addBlog("HL", "With+Image+" + System.nanoTime(), "fdfgdhgfhfhjfhjfhj");
			blogCreator.addBlog("MS", "Hello+Nikhil" + System.nanoTime(), "646545645645ffgfdgdhgdh");
			blogCreator.addBlog("HL", "Java+is+good" + System.nanoTime(), "234rfdsfgdtrgfdbvfdy");
			blogCreator.addBlog("MS", "My+Page" + System.nanoTime(), "vcxftvcbxcdasdaf");
			count--;
		}
	}

	protected void populatePageViewer(PageViewer pageViewer) {
		pageViewer.addPage("HL", "With+Images");
		pageViewer.addPage("MS", "Hello+Nikhil");
		pageViewer.addPage("HL", "Java+is+good");
		pageViewer.addPage("MS", "My+Page");
	}

	protected void convertToKotoFormat() throws IOException {
		long ts = getTimestamp();
		FileWriter fw = new FileWriter(new File("./target/koto-report.log"));
		fw.write("default||na||0||View Page||" + ts + "||" + pageViewer.getMeanInNS() + "||");
		fw.write(System.lineSeparator());
		fw.write("default||na||0||View Page with macros||" + ts + "||" + pageViewerMacro.getMeanInNS() + "||");
		fw.write(System.lineSeparator());
		fw.write("default||na||0||View Space||" + ts + "||" + spaceViewer.getMeanInNS() + "||");
		fw.write(System.lineSeparator());
		fw.write("default||na||0||View Dashboard||" + ts + "||" + dashboardViewer.getMeanInNS() + "||");
		fw.write(System.lineSeparator());
		fw.write("default||na||0||Create Page||" + ts + "||" + pageCreator.getMeanInNS() + "||");
		fw.write(System.lineSeparator());
		fw.write("default||na||0||Create Page with macros||" + ts + "||" + pageCreatorMacros.getMeanInNS() + "||");
		fw.write(System.lineSeparator());
		fw.write("default||na||0||Create Blog||" + ts + "||" + blogCreator.getMeanInNS() + "||");
		fw.write(System.lineSeparator());
		fw.write("default||na||0||Space Browser||" + ts + "||" + spaceBrowser.getMeanInNS() + "||");
		fw.write(System.lineSeparator());
		fw.write("default||na||0||Create Space||" + ts + "||" + spaceCreator.getMeanInNS() + "||");
		fw.write(System.lineSeparator());
		fw.write("default||na||0||Search||" + ts + "||" + searcher.getMeanInNS() + "||");
		fw.write(System.lineSeparator());
//		 fw.write("default||na||0||Login||" + ts + "||" + login.getMeanInNS() + "||");
//		 fw.write(System.lineSeparator());
//		 fw.write("default||na||0||Logout||" + ts + "||" +
//		 logout.getMeanInNS() + "||");
//		 fw.write(System.lineSeparator());
		fw.write("default||na||0||View Page-Warm Up||" + ts + "||" + pageViewerWarmUp.getMeanInNS() + "||");
		fw.write(System.lineSeparator());
		fw.write("default||na||0||View Page with macros Warm Up||" + ts + "||" + pageViewerMacroWarmUp.getMeanInNS() + "||");
		fw.write(System.lineSeparator());
		fw.write("default||na||0||View Space-Warm Up||" + ts + "||" + spaceViewerWarmUp.getMeanInNS() + "||");
		fw.write(System.lineSeparator());
		fw.write("default||na||0||View Dashboard-Warm Up||" + ts + "||" + dashboardViewerWarmUp.getMeanInNS() + "||");
		fw.write(System.lineSeparator());
		fw.write("default||na||0||Create Page-Warm Up||" + ts + "||" + pageCreatorWarmUp.getMeanInNS() + "||");
		fw.write(System.lineSeparator());
//		fw.write("default||na||0||Create Page with macros Warm Up||" + ts + "||" + pageCreatorMacrosWarmUp.getMeanInNS() + "||");
//		fw.write(System.lineSeparator());
		fw.write("default||na||0||Create Blog-Warm Up||" + ts + "||" + blogCreatorWarmUp.getMeanInNS() + "||");
		fw.write(System.lineSeparator());
		fw.write("default||na||0||Space Browser-Warm Up||" + ts + "||" + spaceBrowserWarmUp.getMeanInNS() + "||");
//		fw.write(System.lineSeparator());
//		fw.write("default||na||0||Create Space-Warm Up||" + ts + "||" + spaceCreatorWarmUp.getMeanInNS() + "||");
//		fw.write(System.lineSeparator());
//		fw.write("default||na||0||Login-Warm Up||" + ts + "||" + loginWarmUp.getMeanInNS() + "||");
//		fw.write(System.lineSeparator());
//		fw.write("default||na||0||Logout-Warm Up||" + ts + "||" + logoutWarmUp.getMeanInNS() + "||");
		fw.close();
	}

	protected long getTimestamp() {
		return Instant.now().getEpochSecond() * 1000 + new Random().nextInt(1000);
	}

	protected void populateCreateSpaceData(SpaceCreator spaceCreator, SpaceViewer spaceViewer, int count) {
		spaceCreator.addSpace("My Space", "MS");
		spaceCreator.addSpace("Hello Space", "HL");
		while (count > 0) {
			String name = "MySpace-" + System.nanoTime();
			String key = "MS" + count;
			spaceCreator.addSpace(name, key);
			spaceViewer.addSpace(name, key);
			count--;
		}
	}
}
