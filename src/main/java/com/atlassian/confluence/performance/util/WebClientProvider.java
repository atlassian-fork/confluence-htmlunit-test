package com.atlassian.confluence.performance.util;

import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;

public class WebClientProvider {
	private static WebClient webClient = null;
	
	private WebClientProvider(){}
	
	public static WebClient getInstance(){
		if(webClient == null){
			synchronized (WebClientProvider.class) {
				webClient = new WebClient(TestConstants.BROWSER);
				webClient.getOptions().setJavaScriptEnabled(false);
				webClient.getOptions().setRedirectEnabled(true);
				webClient.getOptions().setCssEnabled(false);
				webClient.getOptions().setUseInsecureSSL(true);
				webClient.getOptions().setThrowExceptionOnScriptError(false);
				webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
				webClient.setAjaxController(new NicelyResynchronizingAjaxController());
				webClient.getCookieManager().setCookiesEnabled(true);
				webClient.getCache().clear();
			}
		}
		return webClient;
	}
	
	public static void close(){
		if(webClient == null){
			webClient.close();
			webClient = null;
		}
	}
}
