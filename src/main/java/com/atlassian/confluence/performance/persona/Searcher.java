package com.atlassian.confluence.performance.persona;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import com.atlassian.confluence.performance.pageobject.Interactions;
import com.atlassian.confluence.performance.util.CustomLogger;
import com.atlassian.confluence.performance.util.PerformanceRecorder;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class Searcher implements Persona {

	private PerformanceRecorder recoder = new PerformanceRecorder();
	private List<String> searchKeys = new ArrayList<String>();

	public void search(int repeatition, boolean record) throws Exception {
		HtmlPage home = Interactions.gotoHome();
		while (repeatition > 0) {
			for (String query : searchKeys) {
				CustomLogger.getLogger().info("Searching for : " + query);
				try {
					if (record)
						recoder.start();
					Interactions.search(home, query);
				} catch (Exception e) {
					CustomLogger.getLogger().log(Level.SEVERE, "Exception while searching:", e);
				} finally {
					if (record)
						recoder.stop();
				}
				CustomLogger.getLogger().info("Search completed");
				Thread.sleep(10);
			}
			repeatition--;
		}
	}

	@Override
	public long getMeanInMS() {
		return recoder.getMeanMS();
	}

	@Override
	public long getMeanInNS() {
		return recoder.getMeanNS();
	}

	public void addSearchKey(String key) {
		this.searchKeys.add(key);
	}
	
	public void clear() {
		this.searchKeys.clear();
	}

}
