package com.atlassian.confluence.performance.persona;

import com.atlassian.confluence.performance.pageobject.Interactions;
import com.atlassian.confluence.performance.util.CustomLogger;
import com.atlassian.confluence.performance.util.PerformanceRecorder;

public class Logout implements Persona{
	private PerformanceRecorder recoder = new PerformanceRecorder();

	public void logout() throws Exception {
		CustomLogger.getLogger().info("Logging out.");
		recoder.start();
		Interactions.logout();
		recoder.stop();
		CustomLogger.getLogger().info("Logged out.");
	}
	
	@Override
	public long getMeanInMS(){
		return recoder.getMeanMS();
	}

	@Override
	public long getMeanInNS() {
		return recoder.getMeanNS();
	}
}
