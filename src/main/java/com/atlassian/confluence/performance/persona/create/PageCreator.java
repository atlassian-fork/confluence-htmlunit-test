package com.atlassian.confluence.performance.persona.create;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import com.atlassian.confluence.performance.pageobject.Page;
import com.atlassian.confluence.performance.persona.Persona;
import com.atlassian.confluence.performance.util.CustomLogger;
import com.atlassian.confluence.performance.util.PerformanceRecorder;

public class PageCreator implements Persona {
	private PerformanceRecorder recoder = new PerformanceRecorder();
	private List<Page> pages = new ArrayList<Page>();

	public void create(boolean record) throws Exception {
		for (Page page : pages) {
			try {
				CustomLogger.getLogger().info("Creating page " + page.getSpaceKey() + "/" + page.getTitle());
				if (record)
					recoder.start();
				page.create();
			} catch (Exception e) {
				CustomLogger.getLogger().log(Level.SEVERE, "Error while creating a page", e);
			} finally {
				if (record)
					recoder.stop();
			}
			CustomLogger.getLogger().info("Created the page " + page.getSpaceKey() + "/" + page.getTitle());
			Thread.sleep(1000);
		}
	}

	public void addPage(String spaceKey, String title, String content) {
		this.pages.add(new Page(spaceKey, title, content));
	}

	public List<Page> getPages() {
		return this.pages;
	}

	@Override
	public long getMeanInMS() {
		return recoder.getMeanMS();
	}

	@Override
	public long getMeanInNS() {
		return recoder.getMeanNS();
	}
}
