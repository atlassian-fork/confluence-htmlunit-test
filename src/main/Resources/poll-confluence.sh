#!/usr/bin/env bash
# Retries a command a configurable number of times with backoff.

function with_backoff {
  max_attempts=${ATTEMPTS-60}
  timeout=${TIMEOUT-10}
  attempt=0
  exitCode=0

  while (( $attempt < $max_attempts ))
  do
    set +e
    "$@"
    exitCode=$?
    set -e

    if [[ $exitCode == 0 ]]
    then
      break
    fi

    echo "Failure! Retrying in $timeout.." 1>&2
    sleep $timeout
    attempt=$(( attempt + 1 ))
    #timeout=$(( timeout * 2 ))
  done

  if [[ $exitCode != 0 ]]
  then
    echo "You've failed me for the last time! ($@)" 1>&2
  fi

  return $exitCode
}
with_backoff curl 'http://localhost:8080/confluence'
