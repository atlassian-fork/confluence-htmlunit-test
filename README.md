## Overview ##
This repo contains the test code for testing confluence performance. This is based on HtmlUnit library.

## How to run ##
    mvn clean install -Dbase-url="http://localhost:8090/confluence"

